package Yoda;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Input is a sentence
 * Output is the sentence with the words printed in reverse order
 * RECURSIVE
 * Created by Eryka on 11/13/2015.
 */
public class YodaSpeakRecursive {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();

        Scanner in = new Scanner(System.in);
        System.out.print("Enter text and press enter: ");

        String line = in.nextLine();

        Scanner lineScanner = new Scanner(line);

        while (lineScanner.hasNext()) {
            String word = lineScanner.next();
            words.add(word);
        }


        System.out.println(recurse(words, 0));

    }


    public static String recurse(ArrayList<String> words, int i) {
        if (i == words.size() - 1 ) {
            return words.get(i);
        }
        else {
            return recurse(words, i+1) +" " + words.get(i);
        }


    }

}
