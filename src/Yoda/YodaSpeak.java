package Yoda;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Input is a sentence
 * Output is the sentence with the words printed in reverse order
 * ITERATIVE
 * Created by Eryka on 11/13/2015.
 */
public class YodaSpeak {
    public static void main(String[] args) {
        ArrayList<String> words = new ArrayList<>();

        Scanner in = new Scanner(System.in);
        System.out.print("Enter text and press enter: ");

        String line = in.nextLine();
        Scanner lineScanner = new Scanner(line);

        while (lineScanner.hasNext()) {
            String word = lineScanner.next();
                words.add(word);
        }

        iterate(words);
    }

    public static void iterate(ArrayList<String> words) {
        for (int i = words.size() - 1; i >= 0; i--) {
            System.out.print(words.get(i) + " " );
        }
    }


}
