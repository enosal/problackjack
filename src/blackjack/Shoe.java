package blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 * Creates a Shoe that can remove one card from itself at a time
 * Created by Eryka on 11/14/2015.
 */
public class Shoe {
    private ArrayList<Card> shoe;
    private int NUM_OF_DECKS;
    private Random cardGenerator;


    //Initialize a shoe object
    public Shoe() {
        //Create shoe
        shoe = new ArrayList<>();
        createShoe();

        //For dealing cards
        cardGenerator = new Random();

    }

    //Creates a shoe of NUM_OF_DECKS decks
    public void createShoe() {
        NUM_OF_DECKS = 6;
        int RADIX = 10;
        //6 decks in a shoe
        for (int deck = 1; deck <= NUM_OF_DECKS; deck++) {
            //make number cards
            for (int val = 0; val <= 9; val++) {
                shoe.add(new Card(Character.forDigit(val, RADIX), Card.Suit.CLUBS));
                shoe.add(new Card(Character.forDigit(val, RADIX), Card.Suit.DIAMONDS));
                shoe.add(new Card(Character.forDigit(val, RADIX), Card.Suit.HEARTS));
                shoe.add(new Card(Character.forDigit(val, RADIX), Card.Suit.SPADES));

                //skip value 1
                if (val == 0) {
                    val++;
                }
            }

            //face cards
            shoe.add(new Card('J', Card.Suit.CLUBS));
            shoe.add(new Card('Q', Card.Suit.CLUBS));
            shoe.add(new Card('K', Card.Suit.CLUBS));
            shoe.add(new Card('A', Card.Suit.CLUBS));

            shoe.add(new Card('J', Card.Suit.DIAMONDS));
            shoe.add(new Card('Q', Card.Suit.DIAMONDS));
            shoe.add(new Card('K', Card.Suit.DIAMONDS));
            shoe.add(new Card('A', Card.Suit.DIAMONDS));

            shoe.add(new Card('J', Card.Suit.HEARTS));
            shoe.add(new Card('Q', Card.Suit.HEARTS));
            shoe.add(new Card('K', Card.Suit.HEARTS));
            shoe.add(new Card('A', Card.Suit.HEARTS));

            shoe.add(new Card('J', Card.Suit.SPADES));
            shoe.add(new Card('Q', Card.Suit.SPADES));
            shoe.add(new Card('K', Card.Suit.SPADES));
            shoe.add(new Card('A', Card.Suit.SPADES));
        }
    }


    //Get a card from the shoe
    public Card getCard() {
        Card card;
        if (shoe.size() <= 0) {
            createShoe();
        }

        //Randomly select a card to remove
        int cardRemoved = cardGenerator.nextInt(shoe.size());
        card = shoe.get(cardRemoved);

        //Remove from shoe
        shoe.remove(cardRemoved);

        return card;
    }




}
