package blackjack;

import java.util.ArrayList;

/**
 *
 * Created by Eryka on 11/14/2015.
 */
public class Player {
    private ArrayList<Card> plyrHand;
    private int score;
    private int numAces;

    public Player(){
        plyrHand = new ArrayList<>();
        score = 0;
        numAces = 0;
    }

    //Get hand
    public ArrayList<Card> getPlyrHand() {return plyrHand; }


    //Get a card
    public void dealtCard(Card card) { plyrHand.add(card);   }

    //Clear hand
    public void clearHand() { plyrHand.clear(); }

    //Gets score
    public int getScore() {
        return score;
    }

    //Clears score
    public void clearScore() {
        score = 0;
    }

    //Computes score
    public void computeScore() {
        score = 0;
        for (Card card: plyrHand) {
            score += card.getVal();
        }
    }

    //Counts aces
    public int getAces() {
        numAces = 0;
        for (Card card: plyrHand) {
            if (card.getFace() == 'A') {
                numAces += 1;
            }
        }
        return numAces;
    }


}
