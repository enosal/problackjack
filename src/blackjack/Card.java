package blackjack;

/**
 * Created with IntelliJ IDEA.
 * User: ag
 * Date: 11/16/13
 * Time: 10:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class Card {

    //public static enum
    public static enum Suit {
        CLUBS('C'), DIAMONDS('D'), HEARTS('H'), SPADES('S');
        private char value;

        //you MUST make this constructor private!
        private Suit(char value) {
            this.value = value;
        }

        public char getValue(){
            return value;
        }

    }

    //instance members
    private char mFace;
    private Suit mSuit;

    //constructor
    public Card(char face, Suit suit) {
        mFace = face;
        mSuit = suit;
    }

    @Override
    public String toString() {
        return String.valueOf(mFace) + mSuit.getValue();
    }


    //getters and setters
    public int getVal() {
        char face = getFace();
        int val = -1;

        switch (face) {
            case '2': val = 2; break;
            case '3': val = 3; break;
            case '4': val = 4; break;
            case '5': val = 5; break;
            case '6': val = 6; break;
            case '7': val = 7; break;
            case '8': val = 8; break;
            case '9': val = 9; break;
            case '0': val = 10; break;
            case 'J': val = 10; break;
            case 'Q': val = 10; break;
            case 'K': val = 10; break;
            case 'A': val = 11; break;
        }
        return val;
    }

    public char getFace() {
        return mFace;
    }

    public void setFace(char face) {
        mFace = face;
    }

    public Suit getSuit() {
        return mSuit;
    }

    public void setSuit(Suit suit) {
        mSuit = suit;
    }
}
