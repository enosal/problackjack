package blackjack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Template started from class
 * Table class includes dealer actions since Dealer "manages a table"
 */
public class Table {
    //For display
    private JPanel mPanel;
    private JButton mButtonStartOver;
    private JPanel mPanelDealer;
    private JPanel mPanelPlayer;
    private JPanel mPanelStatus;
    private JLabel scoresLabel;
    private JLabel moneyLabel;
    private JLabel statusLabel;
    private JButton mButtonDeal;
    private JButton mButtonHit;
    private JButton mButtonStay;
    private JPanel mPanelDealerCards;
    private JPanel mPanelPlayerCards;
    private JButton mButtonBet;
    private final static int FRAME_WIDTH = 600;
    private final static int FRAME_HEIGHT = 850;

    //Player & Dealer
    private Player player;
    private Player dealer;

    //Shoes and discarded cards
    private Shoe shoe;

    //Scoring & Gameplay
    private int numAces;
    private String currentTurn;
    private int plyrMoney;
    private final static int BET_AMOUNT= 10;
    private final static int START_AMOUNT= 1000;


    //To start the program
    public static void main(String[] args) {
        //Frame set-up
        JFrame frame = new JFrame("$" + BET_AMOUNT + " Blackjack");
        frame.setContentPane(new Table().mPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    }

    //Restarts a game
    public void startOver() {
        //Creates Shoe
        shoe = new Shoe();

        //Create Dealer & Player
        player = new Player();
        dealer = new Player();

        //Gameplay
        currentTurn = "User";
        plyrMoney = START_AMOUNT;

        //Reset score values
        player.clearScore();
        dealer.clearScore();

        //Reset labels
        scoresLabel.setText("Scores: You have " + player.getScore() + ". Dealer has ?");
        statusLabel.setText("Please make your $" + BET_AMOUNT + " bet.");
        moneyLabel.setText("Your funds: $" + plyrMoney);

        //Sets button statuses
        resetButtons();

        //Clear cards from table
        mPanelDealer.repaint();
        mPanelPlayer.repaint();

    }

    //Reset buttons to the start of a round
    private void resetButtons() {
        mButtonDeal.setEnabled(false);
        mButtonBet.setEnabled(true);
        mButtonHit.setEnabled(false);
        mButtonStay.setEnabled(false);
    }

    //Pass in the graphics context of the jPanel you want to draw on
    protected void drawCards(Graphics g, ArrayList<Card> plyrHand, boolean coverFirst) {
        BufferedImage bufferedImage;
        final int CARD_WIDTH = 165;
        final int CARD_HEIGHT = 241;

        //use this counter to position the cards horizontally
        int nC = 10;
        for (Card card : plyrHand) {
            if (coverFirst) {
                bufferedImage = SoundImageUtils.genBuffImage("/src/blackjack/imgs/back.png", CARD_WIDTH, CARD_HEIGHT);
                coverFirst = false;
            }
            else {
                bufferedImage = SoundImageUtils.genBuffImage("/src/blackjack/imgs/" + card + ".png", CARD_WIDTH, CARD_HEIGHT);
            }

            //nc = x-pos, 100 = y-pos
            g.drawImage(bufferedImage, nC, 0, null);
            nC = nC +30;
        }
        System.out.println();
    }

    //Dealers turn
    public void dealersTurn() {
        currentTurn = "Dealer";
        dealer.computeScore();
        numAces = dealer.getAces();

        while (dealer.getScore() < 17) {
            dealer.dealtCard(shoe.getCard());
            dealer.computeScore();
            check21(dealer, dealer.getScore(), numAces);
            drawCards(mPanelDealerCards.getGraphics(), dealer.getPlyrHand(), false);
        }

        endOfTurn();

    }

    //End of turn logic
    public void endOfTurn() {
        boolean plyrWin = false;
        String labelStat;

        //Figure out who won
        if (dealer.getScore() > 21) {
            labelStat = "Dealer Busted! You Win!";
            plyrWin = true;
        }
        else if (dealer.getScore() >= player.getScore()) {
            labelStat = "Dealer wins!";
        }
        else {
            labelStat = "You win!";
            plyrWin = true;
        }

        //Labels
        scoresLabel.setText("You have " + player.getScore() + ". Dealer has " + dealer.getScore() + ".");
        moneyLabel.setText("Your funds: $" + plyrMoney);
        statusLabel.setText(labelStat);

        //Won bets
        if (plyrWin) {
            plyrMoney += (2 * BET_AMOUNT);
        }

        //Reset Buttons
        resetButtons();

    }

    //Checks for 21
    public int check21(Player plyr, int score, int numAces) {
        if (score > 21) {
            if (numAces > 0) {
                score -= 10;
                numAces -= 1;
                check21(plyr, score, numAces);
            }
            else {
                if (currentTurn == "User") {
                    statusLabel.setText("You Busted! Dealer wins!");
                    resetButtons();
                }

            }
        }
        else if (score == 21) {
            if (currentTurn == "User") {
                statusLabel.setText("You have 21!");
            }
            else {
                statusLabel.setText("Dealer has 21!");
                resetButtons();
            }
        }
        else { //under 21
            if (currentTurn == "User") {
                statusLabel.setText("Hit or Stay?");
            }
            else {
                statusLabel.setText("Dealer deciding to Hit or Stay");
            }
        }

        return score;
    }

    //Make bet
    public void bet() {
        //Clear scores
        player.clearScore();
        dealer.clearScore();

        //Labels
        scoresLabel.setText("Scores: You have " + player.getScore() + ". Dealer has ?");

        //Clear cards from table
        mPanelDealer.repaint();
        mPanelPlayer.repaint();

        //Check user's money
        if (plyrMoney <= 0) {
            statusLabel.setText("You have no more money left! Time to stop gambling!");
            mButtonBet.setEnabled(false);
            mButtonDeal.setEnabled(false);
            mButtonHit.setEnabled(false);
            mButtonStay.setEnabled(false);
        }
        else {
            plyrMoney -= BET_AMOUNT;
            mButtonBet.setEnabled(false);
            mButtonDeal.setEnabled(true);
            statusLabel.setText("You have bet $" + BET_AMOUNT + ". Notify dealer that you are ready to start");
            moneyLabel.setText("Your funds: $" + plyrMoney);
        }
    }

    //Deals cards to start a round
    public void deal() {
        //Empty hands
        player.clearHand();
        dealer.clearHand();

        //Removes cards from table
        mPanelPlayerCards.removeAll();
        mPanelDealerCards.removeAll();

        //Two Cards for Player
        player.dealtCard(shoe.getCard());
        player.dealtCard(shoe.getCard());

        //Two Cards for Dealer
        dealer.dealtCard(shoe.getCard());
        dealer.dealtCard(shoe.getCard());

        //disable Deal button, enable user buttons
        mButtonDeal.setEnabled(false);
        mButtonHit.setEnabled(true);
        mButtonStay.setEnabled(true);

        //Draw cards
        drawCards(mPanelDealerCards.getGraphics(), dealer.getPlyrHand(), true);
        drawCards(mPanelPlayerCards.getGraphics(), player.getPlyrHand(), false);

        //Gameplay
        player.computeScore();
        currentTurn = "User";

        System.out.println(player.getScore());
        //Labels
        scoresLabel.setText("Scores: You have " + player.getScore() +". Dealer has ?");
        statusLabel.setText("Hit or Stay?");
    }

    //Constructs a table
    public Table() {
        //Starts over
        startOver();

        /*
        Event Listeners
         */

        //Dealer Deals
        mButtonDeal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deal();
            }
        });

        //User Hits
        mButtonHit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                player.dealtCard(shoe.getCard());
                player.computeScore();
                check21(player, player.getScore(), player.getAces());
                scoresLabel.setText("Scores: You have " + player.getScore() +". Dealer has ?");
                drawCards(mPanelPlayerCards.getGraphics(), player.getPlyrHand(), false);

            }
        });

        //User Stays
        mButtonStay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                player.computeScore();
                check21(player, player.getScore(), player.getAces());
                mButtonHit.setEnabled(false);
                mButtonStay.setEnabled(false);

                scoresLabel.setText("Scores: You have " + player.getScore() +". Dealer has ?");

                dealersTurn();
            }
        });

        //User bets
        mButtonBet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bet();
            }
        });

        //Reset Game
        mButtonStartOver.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startOver();
            }
        });

    }

}
