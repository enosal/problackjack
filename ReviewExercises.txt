#########################################################################
# Use this file to answer Review Exercises from the Big Java textbook
#########################################################################

//R13.1 Terms
/a. Recursion is when a function or computation calls itself but with a simpler input.
/b. Iteration is to repeat the same function or computation on each element involved (in an array, a character in a string, etc).
/c. Infinite Recursion is when a function continuously calls itself and there is no terminating case (or base-case).
/d. Recursive Helper method changes the original problem slightly. The recursive method calls the recursive helper method to aid it

//R13.2 Smallest value
smallestVal(array, start index, end index)
if the start index is the same as the end index, then the smallest value is the value at that index. return that value
otherwise: call the function smallestVal on the start index to the middle index (ceiling). call the function smallestVal on the middle index to the end index. compare these two. return the smallest value

//R13.3 Sort array of numbers
smallestValIndex: alter smallestVal (from 13.2) so that it return the index of the smallest value.

sort(array, start index, end index)
if start index = end index, return the array
otherwise, call smallestValIndex. Swap the value at this index with value at the start index (a temporary variable needs to be used, of course). recurse on the array starting with the same end index but incrementing the start index by one

(the array is being mutated as we go along)



//R13.6 Exponents recursively
The recursion terminates when n is 0 (anything to the 0th power is 1).
exp(x,n)
if n is 0, return 1
otherwise: return x * exp(x, n-1)

To be a bit more like the Fibonacci example:
exp(x,n)
if n is 1, return x.
otherwise, return exp(x,1) * exp(x, n-1)



//R13.8 Factorial recursively
fact(n)
if n is 1, return n.
otherwise, return n * fact(n-1)


